package com.hotmail.steven;
import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;
import java.util.Iterator;


public class ChatServer {
    
	/**
	 * The time in seconds for the thread to
	 * check for disconnected users
	 */
	private final int DISCONNECT_TIME = 2;
	// List of connected clients in this server
	private  static ArrayList<ClientHandler> clientHandlers;
    
    public static void main(String[] args)
    {
    	
    	// Entry point into the program
    	new ChatServer();
    	
    }
    
    public ChatServer()
    {
    	ServerSocket server = null;
    	// How long ago we checked for disconnected users
    	startAnnouncer();
        try {
            server = new ServerSocket(5000);
            System.out.println("Server setup: waiting for client connections");
            
            clientHandlers = new ArrayList<ClientHandler>();
            
            while(true)
            {
                // keep processing and accepting client connections forever
                Socket serverSocket = server.accept();
                
                ClientHandler newClientHandler = new ClientHandler(serverSocket,clientHandlers);
                newClientHandler.start();
                
                clientHandlers.add(newClientHandler);
                
                // Update the connected lists for each client
                for(ClientHandler handler : clientHandlers)
                {
                	handler.clients = clientHandlers;
                }
            }
            
        }
        catch (IOException e)
        {
            e.printStackTrace();
        }
    }
    
    /**
     * Request the server update its user list
     */
    public static void checkDisconnectedUsers()
    {
    	System.out.println("chedck");
    	for(ClientHandler clientHandler : clientHandlers)
    	{
    		if(clientHandler.serverSocket.isClosed() || !clientHandler.serverSocket.isConnected())
    		{
    			System.out.println("dc for " + clientHandler.clientName);
    		}
    	}
    }
    
    /**
     * Notify the server that a client
     * was disconnected on a port
     * @param port
     */
    public static void notifyDisconnect(int port)
    {
    	System.out.println("notified from "  +port);
    	for(int i=0;i<clientHandlers.size();i++)
    	{
    		ClientHandler handler = clientHandlers.get(i);
    		if(handler.serverSocket.getPort() == port)
    		{
    			handler.interrupt();
    			clientHandlers.remove(i);
    		}
    	}
    }
    
    /**
     * Start the server announcement system
     */
    private void startAnnouncer()
    {
    	
    }
    
    // Get a connected client on a specified port
    public ClientHandler getHandler(int port)
    {
    	Iterator<ClientHandler> itrHandler = clientHandlers.iterator();
    	while(itrHandler.hasNext())
    	{
    		ClientHandler next = itrHandler.next();
    		if(next.serverSocket.getPort() == port)
    		{
    			return next;
    		}
    	}
    	return null;
    }
    
    // Broadcast a message to all connected clients
    public void broadcastMessage(String message)
    {
    	Iterator<ClientHandler> itrHandler = clientHandlers.iterator();
    	while(itrHandler.hasNext())
    	{
    		ClientHandler next = itrHandler.next();
    		next.sendMessage(message);
    	}
    }
}

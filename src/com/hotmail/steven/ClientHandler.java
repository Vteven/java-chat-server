package com.hotmail.steven;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.Socket;
import java.util.ArrayList;


public class ClientHandler extends Thread {
    Socket serverSocket;
    ArrayList<ClientHandler> clients;
    
    DataInputStream dis = null;
    DataOutputStream dos = null;
    
    String clientName = null;
    
    public ClientHandler(Socket serverSocket, ArrayList<ClientHandler> clients)
    {
        this.serverSocket = serverSocket;
        this.clients = clients;
        
        System.out.println("New client connected"+serverSocket.getRemoteSocketAddress().toString());
        
        OutputStream os = null;
        InputStream in = null;
        try {
            in = serverSocket.getInputStream();
            os = serverSocket.getOutputStream();
        } catch (IOException e) {
            e.printStackTrace();
        }
        
        // the raw input stream only deals with bytes so lets wrap it in a data input stream
        
        dis = new DataInputStream(in);            
        dos = new DataOutputStream(os);

    }

    public void run()
    {
        System.out.println("Client connected");
        
        while(true)
        {
            int message;
            try {
                message = dis.read();
                //TODO block and wait for the client to send an appropriate message
            
                // decode the message based on message id
                if(message == ServerConstants.CHAT_MESSAGE)
                {
                	String chatMsg = dis.readUTF();
                	for(ClientHandler clientHandler : clients)
                	{
                		try
                		{
	                		clientHandler.dos.write(ServerConstants.BROADCAST_MESSAGE);
	                		clientHandler.dos.writeUTF(clientName + "> " + chatMsg);
	                		System.out.println(chatMsg);
	                		
	                		dos.flush();
                		
                		} catch(IOException e)
                		{
                			ChatServer.notifyDisconnect(clientHandler.serverSocket.getPort());
                		}
                	}
                }
                else if(message == ServerConstants.SETUP_MESSAGE)
                {
                    clientName = dis.readUTF();
                    System.out.println("Client Name:"+clientName);
                    
                    // ack that we have recieved the client's name
                    dos.write(ServerConstants.SETUP_ACK);
                } else if(message == ServerConstants.BROADCAST_MESSAGE)
                {
                	dos.write(ServerConstants.BROADCAST_CLIENT);
                	// Hold the update to be sent to the client
                	StringBuilder payload = new StringBuilder();
                	for(ClientHandler handler : clients)
                	{
                		payload.append("," + handler.clientName + ":" + handler.serverSocket.getPort());
                	}
                	dos.writeUTF(payload.toString().replaceFirst(",", ""));
                }
            
            } catch (IOException e) {
                // TODO Auto-generated catch block
                //e.printStackTrace();
            	ChatServer.notifyDisconnect(serverSocket.getPort());
            	break;
            } 
    }
    }
    
    // Send a message to this client
    public void sendMessage(String message)
    {
    	try {
			dos.write(ServerConstants.BROADCAST_MESSAGE);
			dos.writeUTF(message);
			
			dos.flush();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    }
}

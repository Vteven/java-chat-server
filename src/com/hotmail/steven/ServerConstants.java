package com.hotmail.steven;

public class ServerConstants {
    public static final int CHAT_MESSAGE = 0;
    public static final int SETUP_MESSAGE = 1;
    
    public static final int SETUP_ACK = 2;
    public static final int CHAT_ACK = 3;
    
    public static final int UPDATE_CLIENT = 4;
    public static final int BROADCAST_CLIENT = 5;
    
    public static final int BROADCAST_MESSAGE = 6;
}
